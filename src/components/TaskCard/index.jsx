import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import Chip from '../TaskStatusTag';
// import Stack from '@mui/material/Stack';
// import CustomButton from '../CustomButton';

export default function TaskCard() {
  return (
    <Card sx={{ width: 300, height: 220, display: 'flex', flexDirection: 'column' }}>
      <CardContent
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}
      >
        <Typography sx={{ fontSize: 24 }}>Cleaning</Typography>
        <Typography sx={{ fontSize: 25 }}>$4000</Typography>
      </CardContent>
      <CardContent sx={{ display: 'flex', flexDirection: 'column' }}>
        <Typography>3000, NSW</Typography>
        <Typography>Tuesday, 5 April 2022</Typography>
      </CardContent>
      <CardContent
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '150px',
        }}
      >
        <Chip label="Open" color="primary" />
        <FavoriteIcon color="error" fontSize="large" />
      </CardContent>
    </Card>
  );
}
