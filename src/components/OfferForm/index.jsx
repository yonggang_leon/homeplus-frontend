import React from 'react';
import { Box } from '@mui/system';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import CustomButton from '../CustomButton';
import { BoxStyle, ButtomPosition } from './OfferForm.styled';

const OfferForm = () => {
  return (
    <div>
      <BoxStyle>
        <Box
          component="form"
          sx={{
            mt: '10px',
            mb: '10px',
            display: 'flex',
            flexDirection: 'row',
            '& > :not(style)': { width: '15ch' },
          }}
          noValidate
          autoComplete="yes"
        >
          <Typography sx={{ mt: '20px', fontSize: '12px' }}> MY OFFER </Typography>
          <TextField
            type={'number'}
            onChange={(event) => (event.target.value < 0 ? (event.target.value = 0) : event.target.value)}
            variant="outlined"
            style={{ backgroundColor: '#E5E5E5' }}
          />
        </Box>

        <Typography sx={{ fontSize: '12px' }}> MESSAGE </Typography>
        <TextareaAutosize
          maxRows={4}
          aria-label="maximum height"
          placeholder="Contents"
          style={{ height: 100, width: 200, backgroundColor: '#E5E5E5' }}
        />
        <div>
          <Box>
            <ButtomPosition>
              <CustomButton size="small" variant="contained">
                Submit
              </CustomButton>
            </ButtomPosition>
          </Box>
        </div>
      </BoxStyle>
    </div>
  );
};

export default OfferForm;
