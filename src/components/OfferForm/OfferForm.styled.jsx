import styled from 'styled-components';
import { Box } from '@mui/system';

export const BoxStyle = styled(Box)`
  height: 250px;
  width: 220px;
  border: 2px solid black;
  background-color: white;
  padding: 20px;
`;
export const ButtomPosition = styled.div`
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: flex-end;
`;
