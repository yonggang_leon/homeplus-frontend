import React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ImageAvatars from '../Profile-piconclick';
import CustomButton from '../CustomButton';

const OfferCard = ({ name, price, contents, date }) => {
  return (
    <Box
      sx={{
        width: '300px',
        height: '150px',
        mt: '30px',
        backgroundColor: '#E5E5E5',
        padding: '10px',
      }}
    >
      <div>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}
        >
          <ImageAvatars />
          <Typography>{name}</Typography>
          <Typography>{price}</Typography>
        </Box>
      </div>
      <div>
        <div>
          <p
            style={{
              wordWrap: 'break-word',
              overflowY: 'scroll',
              border: '2px solid #E5E5E5',
              width: '260px',
              height: '50px',
              margin: '20px',
            }}
          >
            {contents}
          </p>
        </div>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            height: '20px',
            justifyContent: 'space-around',
            alignItems: 'flex-end',
          }}
        >
          <Box>
            <CustomButton size="small" variant="contained">
              Accept
            </CustomButton>
            <CustomButton size="small" variant="outlined">
              Reject
            </CustomButton>
          </Box>
          <div>
            <Typography>{date}</Typography>
          </div>
        </Box>
      </div>
    </Box>
  );
};

OfferCard.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  contents: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};

export default OfferCard;
