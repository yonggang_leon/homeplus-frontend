import React from 'react';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import Link from '@mui/material/Link';
import pic from '../../assets/defaultAvatar.jpg';

const ImageAvatars = ({ link }) => (
  <Link href={`/${link}`}>
    <Stack direction="row" spacing={2}>
      <Avatar alt="pic" src={pic} />
    </Stack>
  </Link>
);

export default ImageAvatars;
