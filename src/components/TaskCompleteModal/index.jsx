import React, { useRef, useEffect, useCallback } from 'react';
import { useSpring, animated } from 'react-spring';
import PropTypes from 'prop-types';
import Rating from '@mui/material/Rating';
import Box from '@mui/material/Box';
import StarIcon from '@mui/icons-material/Star';
import DefaultImage from '../../assets/defaultUser.PNG';
import {
  Wrapper,
  Background,
  ModalWrapper,
  ModalContent,
  CloseModalButton,
  MenuLinkAvatar,
  UsernameText,
  Text,
} from './TaskCompleteModal.style.jsx';

const labels = {
  0.5: 'Useless',
  1: 'Useless+',
  1.5: 'Poor',
  2: 'Poor+',
  2.5: 'Ok',
  3: 'Ok+',
  3.5: 'Good',
  4: 'Good+',
  4.5: 'Excellent',
  5: 'Excellent+',
};

const TaskCompleteModal = ({ setShowModal, showModal, task }) => {
  const modalRef = useRef();
  const [value, setValue] = React.useState(2);
  const [hover, setHover] = React.useState(-1);
  const animation = useSpring({
    config: {
      duration: 250,
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`,
  });

  //TODO: get task from backend and use the tasker_id and the user_id to find the information of the tradie
  task = {
    rating: 0,
  };

  const tradieInfo = {
    username: 'Ken',
    avatar: DefaultImage,
  };

  const taskerInfo = {
    bank_bsb: '010010',
    bank_account: '12345678',
  };

  const closeModal = (e) => {
    if (modalRef?.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    (e) => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
      }
    },
    [setShowModal, showModal]
  );

  useEffect(() => {
    document.addEventListener('keydown', keyPress);
    return () => document.removeEventListener('keydown', keyPress);
  }, [keyPress]);

  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalWrapper showModal={showModal}>
              <ModalContent>
                <Wrapper>
                  <MenuLinkAvatar src={tradieInfo.avatar} href="/" />
                  <UsernameText>{tradieInfo.username}</UsernameText>
                  <Text>BSB: {taskerInfo.bank_bsb} </Text>
                  <Text>Bank account: {taskerInfo.bank_account} </Text>
                  <Box
                    sx={{
                      width: 200,
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Rating
                      name="hover-feedback"
                      value={value}
                      precision={0.5}
                      size="large"
                      onChange={(event, newValue) => {
                        setValue(newValue);
                      }}
                      onChangeActive={(event, newHover) => {
                        setHover(newHover);
                      }}
                      emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
                    />
                    {value !== null && <Box sx={{ ml: 2 }}>{labels[hover !== -1 ? hover : value]}</Box>}
                  </Box>
                </Wrapper>
              </ModalContent>
              <CloseModalButton aria-label="Close modal" onClick={() => setShowModal((prev) => !prev)} />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
    </>
  );
};

TaskCompleteModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
  task: PropTypes.object,
};

export default TaskCompleteModal;
