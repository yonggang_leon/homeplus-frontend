import styled, { keyframes } from 'styled-components';
import Close from '@mui/icons-material/Close';

const jump = keyframes`
    from{
    transform: translateY(0)
    }
    to{
    transform: translateY(-3px)
    }
`;

export const Wrapper = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;
  width: 100%;
  font-family: roboto;
`;

export const Background = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.8);
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ModalWrapper = styled.div`
  width: 300px;
  height: 350px;
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
  background: #fff;
  color: #000;
  display: grid;
  position: center;
  z-index: 10;
  border-radius: 10px;
`;

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 1.8;
  color: #141414;
  p {
    margin-bottom: 1rem;
  }
`;

export const CloseModalButton = styled(Close)`
  cursor: pointer;
  position: absolute;
  top: 20px;
  right: 20px;
  width: 32px;
  height: 32px;
  padding: 0;
  z-index: 10;
`;

export const Text = styled.div`
  color: #444;
  align-items: center;
  font-size: 17px;
  display: flex;
`;

export const UsernameText = styled.div`
  color: #444;
  align-items: center;
  font-size: 20px;
  display: flex;
  font-weight: 900;
`;

export const MenuLinkAvatar = styled.img`
  margin: 10px;
  max-width: 55px;
  width: 50px;
  height: 50px;
  object-fit: cover;
  border-radius: 50%;
`;

Wrapper.displayName = 'Wrapper';
ModalWrapper.displayName = 'ModalWrapper';
ModalContent.displayName = 'ModalContent';
CloseModalButton.displayName = 'CloseModalButton';
Text.displayName = 'Text';
UsernameText.displayName = 'UsernameText';
Background.displayName = 'Background';
UsernameText.displayName = 'UsernameText';
MenuLinkAvatar.displayName = 'MenuLinkAvatar';
