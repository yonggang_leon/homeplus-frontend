import { React, useState } from 'react';
import TextField from '@mui/material/TextField';

import FaceIcon from '@mui/icons-material/Face';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import EventIcon from '@mui/icons-material/Event';
import MapsHomeWorkIcon from '@mui/icons-material/MapsHomeWork';
import HotelIcon from '@mui/icons-material/Hotel';
import ShowerIcon from '@mui/icons-material/Shower';
import TaskStatusTag from '../TaskStatusTag';
import InfoCard from '../InformationCard';
import CustomButton from '../CustomButton';
import CommentBox from '../CommentBox';

import { DetailCard } from './TaskDetailCard.style';

const leaveComment = (comment) => {
  return <CommentBox id="1" message={comment} username="John Smith" timeStamp="13:00, 12 Feb" />;
};

const TaskDetailCard = () => {
  const [comment, setComment] = useState();
  const [sendComment, setSendComment] = useState();

  const handleChange = (e) => {
    e.preventDefault();
    setComment(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (comment !== '') setSendComment(leaveComment(comment));
  };

  return (
    <DetailCard>
      <div className="cardHeader">
        <div className="cardTitle">
          <h2>House Cleaner</h2>
          <TaskStatusTag label="open" color="primary" />
        </div>
        <InfoCard title="TASK BUDGET" content="$4,000" />
        <CustomButton color="primary" variant="contained">
          Make an offer
        </CustomButton>
      </div>
      <div className="cardImportInfo">
        <InfoCard title="POSTED BY" content="Scoot Morrison" icon={<FaceIcon />} />
        <div className="shortLine" />
        <InfoCard title="LOCATION" content="7000 Hobart, TAS" icon={<LocationOnIcon />} />
        <div className="shortLine" />
        <InfoCard title="DATE & TIME" content="22 Feb 2022, 1pm - 5pm" icon={<EventIcon />} />
      </div>
      <div className="longLine" />
      <div className="cardOtherInfo">
        <div className="taskInfo">
          <div className="taskSubInfo">
            <InfoCard title="Type" content="House" icon={<MapsHomeWorkIcon />} />
            <InfoCard title="Rooms" content="2" icon={<HotelIcon />} />
            <InfoCard title="Bathrooms" content="2" icon={<ShowerIcon />} />
          </div>
          <div className="description">
            <span>I need some one to clean my house.</span>
          </div>
        </div>
        <div className="commentArea">
          <form onSubmit={handleSubmit}>
            <div className="commentBox">
              <TextField
                id="standard-basic"
                label="comment"
                variant="standard"
                sx={{ width: '70%' }}
                onChange={handleChange}
              />
              <CustomButton color="primary" variant="outlined" size="small" type="submit">
                send
              </CustomButton>
            </div>
          </form>
          {sendComment ? sendComment : null}
          <CommentBox
            id={1}
            message="including balcony?fwefwefwqswcfqwdqwdqwdqwdefwefwefwefwefwefwefwefwef"
            username="John Smith"
            timeStamp="13:00, 2 Feb"
          />
          <CommentBox
            id={1}
            message="including balcony?fwefwefwqswcfqwdqwdqwdqwdefwefwefwefwefwefwefwefwef"
            username="John Smith"
            timeStamp="13:00, 2 Feb"
          />
          <CommentBox
            id={1}
            message="including balcony?fwefwefwqswcfqwdqwdqwdqwdefwefwefwefwefwefwefwefwef"
            username="John Smith"
            timeStamp="13:00, 2 Feb"
          />
        </div>
      </div>
    </DetailCard>
  );
};

export default TaskDetailCard;
