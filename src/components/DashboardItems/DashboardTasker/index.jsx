import React from 'react';
import DashboardCard from '../DashboardCard';
import DashboardNotification from '../DashboardNotification';

const DashboardTasker = () => (
  <>
    <DashboardCard title="Assigned Tasks" count={1} theme="primary" />
    <DashboardCard title="Pending Offers" count={2} theme="secondary" />
    <DashboardNotification>Home Cleaner has received new offers</DashboardNotification>
  </>
);
export default DashboardTasker;
