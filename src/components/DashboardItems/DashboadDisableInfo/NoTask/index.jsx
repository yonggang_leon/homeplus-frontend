import * as React from 'react';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import pic from '../../../../assets/EmptyBox.jpg';

import CustomButton from '../../../CustomButton';

const Disable = ({ link }) => {
  return (
    <div>
      <Box>
        <Box
          sx={{
            mt: '200px',
            display: 'flex',
            flexDirection: 'row',
            width: '95%',
            justifyContent: 'center',
            color: '#444',
          }}
        >
          <img src={pic} alt="Logo" />
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            width: '95%',
            justifyContent: 'center',
            color: '#444',
          }}
        >
          <p> You dont have a task yet!</p>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            width: '95%',
            justifyContent: 'center',
          }}
        >
          <Link underline="none" href={`/${link}`}>
            <CustomButton color="primary" variant="outlined">
              Browse tasks
            </CustomButton>
          </Link>
        </Box>
      </Box>
    </div>
  );
};

export default Disable;
