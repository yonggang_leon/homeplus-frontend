import * as React from 'react';
import Box from '@mui/material/Box';
import { TextStyle } from './Text.styled';
import DashboardNotification from '../DashboardNotification';
import DashboardCard from '../DashboardCard';

export default function DashboardHome() {
  return (
    <Box>
      <TextStyle> As a Client </TextStyle>
      <DashboardCard title="Upcoming tasks" count={4} theme="primary" />
      <TextStyle>As a Tasker</TextStyle>
      <DashboardCard title="Upcoming tasks" count={2} theme="secondary" />
      <DashboardNotification>Home Cleaner has received new offers</DashboardNotification>
    </Box>
  );
}
