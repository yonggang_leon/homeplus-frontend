import { combineReducers } from 'redux';

import taskFormReducer from './form/form.reducer';

const reducer = combineReducers({
  taskForm: taskFormReducer,
});

export default reducer;
