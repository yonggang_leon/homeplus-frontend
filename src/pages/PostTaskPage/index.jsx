import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import axios from 'axios';
import PostForm from '../../components/TaskForm';

// const api = axios.create({
//   baseURL: 'http://localhost:8080/api/v1',
// });

const PostTaskPage = ({ values }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    alert(JSON.stringify(values, null, 2));
    // let res = api.post('/post-task', { user_id: 1, ...values });
    // console.log(res);
  };

  return <PostForm handleSubmit={handleSubmit} />;
};

PostTaskPage.propTypes = {
  values: PropTypes.object.isRequired,
};

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps)(PostTaskPage);
