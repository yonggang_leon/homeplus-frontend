import styled from 'styled-components';
export const Box = styled.div`
  width: 100%;
  height: 100vh;
  h3 {
    font-family: Roboto;
    font-size: 1.8rem;
    font-weight: 300;
    width: 100%;
    text-align: center;
    position: relative;
    top: 6rem;
    margin-bottom: 3rem;
  }
`;
export const Container = styled.div`
  position: relative;
  display: flex;
  width: 70%;
  top: 8rem;
  left: 50%;
  transform: translateX(-50%);
  margin-bottom: 10rem;
`;
