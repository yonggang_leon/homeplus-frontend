import styled from 'styled-components';

export const Box = styled.div``;
export const StepStyle = styled.div`
  background: #438d7d;
  border-radius: 50%;
  width: 5rem;
  height: 5rem;
  margin: auto;
`;
export const StepNum = styled.div`
  font-family: Roboto;
  text-align: center;
  font-size: 3rem;
  color: #ffffff;
  position: relative;
  top: 10%;
`;
export const StepDescription = styled.div`
  font-family: Roboto;
  font-size: 1.3rem;
  line-height: 1.5rem;
  color: #444;
  justify-content: center;
  padding-top: 50px;
  text-align: center;
`;
