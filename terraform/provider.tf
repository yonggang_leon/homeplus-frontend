terraform {
   required_providers {
    aws = {
         source = "hashicorp/aws"
         version =  "~> 3.74.2"
    }
    consul = {
      source = "hashicorp/consul"
    }
  }
  required_version = ">= 1.1.7"
}

provider "aws" {
  region = var.region
}
