module "mywebsite" {
  source      = "../terraform_modules"
  endpoint    = "h.handymanp3.com"
  domain_name = "handymanp3.com"
  region      = var.region
  bucket_name = "h.handymanp3.com"
}
